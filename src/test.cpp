#include "ros/ros.h"
#include "polar_2d_map_representation/polar_2d_map_representation.h"

namespace ca_ri = ca::representation_interface;

int main(int argc, char **argv)
{
    ros::init(argc, argv, "talker");
    ros::NodeHandle n;


    ros::Publisher marker_pub = n.advertise<visualization_msgs::Marker>("polar_representation", 1000);
    ros::Rate loop_rate(10);

    ca_ri::Polar_2D_Representation  cylinder_rep;
    Eigen::Vector3d center(0,0,0);
    cylinder_rep.SetParams((M_PI/4), 5, (2*M_PI),0,center,5.0,std::string("/world"));
    int count = 0;
    visualization_msgs::Marker m;
    while (ros::ok())
    {
        m = cylinder_rep.GetCylinder(0);
        marker_pub.publish(m);

        m = cylinder_rep.GetSectors(0);
        marker_pub.publish(m);
        Eigen::Vector3d v1(0,5.5,-1);
        Eigen::Vector3d v2(0,0,-7);
        Eigen::Vector3d v3(0,0,7);
        if(cylinder_rep.FindIntersection(v2,v3,4,v1)){
            ROS_ERROR_STREAM("Oh yes::"<<v1);
        }else
            ROS_ERROR_STREAM("NO");

        size_t id;
        if(cylinder_rep.GetIDs(Eigen::Vector3d(1,1,-3),id)){
    //        ROS_ERROR_STREAM("Got an ID");
            cylinder_rep.SetID(id,1);
        }
        ros::spinOnce();
        loop_rate.sleep();
        ++count;
    }


    return 0;
}
