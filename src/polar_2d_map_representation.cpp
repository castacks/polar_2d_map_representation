#include "polar_2d_map_representation/polar_2d_map_representation.h"

namespace ca_ri = ca::representation_interface;
visualization_msgs::Marker ca_ri::Polar_2D_Representation::GetCylinder(int id){
    visualization_msgs::Marker m;
    m.header.frame_id = frame_;
    m.header.stamp = ros::Time();
    m.ns = "cylinder";
    m.frame_locked = true;
    m.id = id;
    m.type = visualization_msgs::Marker::CYLINDER;
    m.action = visualization_msgs::Marker::ADD;
    m.pose.position.x = center_.x();
    m.pose.position.y = center_.y();
    m.pose.position.z = center_.z()- 0.5*height_;
    m.pose.orientation.x = 0.0;
    m.pose.orientation.y = 0.0;
    m.pose.orientation.z = 0.0;
    m.pose.orientation.w = 1.0;
    m.scale.x = 2*max_radius_;
    m.scale.y = 2*max_radius_;
    m.scale.z = height_;
    m.scale.z = height_;
    m.color.a = 1.0; // Don't forget to set the alpha!
    m.color.r = 1.0;
    m.color.g = 0.0;
    m.color.b = 0.0;
    //m.points.push_back(p);
    return m;
}
visualization_msgs::Marker ca_ri::Polar_2D_Representation::GetSectors(int id){
    visualization_msgs::Marker m;
    m.header.frame_id = frame_;
    m.header.stamp = ros::Time();
    m.ns = "sectors";
    m.frame_locked = true;
    m.id = id;
    m.type = visualization_msgs::Marker::TRIANGLE_LIST;
    m.action = visualization_msgs::Marker::ADD;
    m.pose.position.x = 0;
    m.pose.position.y = 0;
    m.pose.position.z = 0;
    m.pose.orientation.x = 0.0;
    m.pose.orientation.y = 0.0;
    m.pose.orientation.z = 0.0;
    m.pose.orientation.w = 1.0;
    m.scale.x = 1;
    m.scale.y = 1;
    m.scale.z = 1;
    m.color.a = 1.0; // Don't forget to set the alpha!
    m.color.r = 1.0;
    m.color.g = 0.0;
    m.color.b = 0.0;
    double angle = 0;
    Eigen::Vector3d v1, v2, v3;
    for(size_t i=0; i<cells_.size(); i++){
        //ROS_ERROR_STREAM("Center::"<<center_);
        angle = CellID2Angle(i);
        //ROS_ERROR_STREAM("Angle::"<<i<<"::"<<angle);
        double start_angle = angle - 0.5*angle_resolution_;
        double end_angle = angle + 0.5*angle_resolution_;
        v1 = center_;
        v2 = center_ + max_radius_*Eigen::Vector3d(std::cos(start_angle),std::sin(start_angle),0);
        v3 = center_ + max_radius_*Eigen::Vector3d(std::cos(end_angle),std::sin(end_angle),0);

        geometry_msgs::Point p; p.x = v1.x(); p.y = v1.y(); p.z = v1.z();
        m.points.push_back(p);
        p.x = v2.x(); p.y = v2.y(); p.z = v2.z();
        m.points.push_back(p);
        p.x = v3.x(); p.y = v3.y(); p.z = v3.z();
        m.points.push_back(p);

        std_msgs::ColorRGBA c; c.r = 0; c.g = 0; c.b = 0; c.a = std::max(cells_[i],0.1);
        //ROS_ERROR_STREAM("c.a::"<<c.a);
        m.colors.push_back(c);
        m.colors.push_back(c);
        m.colors.push_back(c);
    }
    //m.points.push_back(p);
    return m;
}


bool ca_ri::Polar_2D_Representation::IsInCylinder(Eigen::Vector3d pos){
    if(((center_.z()-pos.z()) <= height_) && (center_.z()>=pos.z())){
        pos.z() = center_.z();
        if(pos.x()==0 && pos.y()==0){
            ROS_INFO_STREAM("It is reaching there");
        }
        if((center_-pos).norm() <= max_radius_ ){
            if(pos.x()==0 && pos.y()==0){
                ROS_INFO_STREAM("It is going to be true");
            }
            return true;
        }
    }
    return false;
}

bool ca_ri::Polar_2D_Representation::FindIntersection(Eigen::Vector3d start_p, Eigen::Vector3d end_p, double radius, Eigen::Vector3d &intersection_point){

    // Making the cylinder center to be 0,0
    Eigen::Vector3d p1 = start_p - center_;
    Eigen::Vector3d p2 = end_p - center_;
    Eigen::Vector3d d = end_p - start_p;
    double D = p1.x()*p2.y() - p2.x()*p1.y();
    double dr = std::pow(d.x(),2) + std::pow(d.y(),2);// std::sqrt(dr);
    double discriminant = radius*radius*dr-D*D;
    Eigen::Vector3d p3,p4;
    if(discriminant<0)
        return false;
    else if(discriminant == 0){
        p3.x() = D*d.y()/dr;
        p3.y() = -D*d.x()/dr;
        p4 = p3;
    }
    else{
        discriminant = std::sqrt(discriminant);
        if(d.y() < 0){
            p3.x() = (D*d.y() + (-1)*d.x()*discriminant)/dr;
            p3.y() = (-D*d.x() + std::abs(d.y())*discriminant)/dr;

            p4.x() = (D*d.y() - (-1)*d.x()*discriminant)/dr;
            p4.y() = (-D*d.x() - std::abs(d.y())*discriminant)/dr;

        }else{
            p3.x() = (D*d.y() + d.x()*discriminant)/dr;
            p3.y() = (-D*d.x() + std::abs(d.y())*discriminant)/dr;

            p4.x() = (D*d.y() - d.x()*discriminant)/dr;
            p4.y() = (-D*d.x() - std::abs(d.y())*discriminant)/dr;
        }
    }
    p3 = p3 + center_;
    p4 = p4 + center_;
    if(std::abs(d.x())>0){
        double lambda = (p3.x() - start_p.x())/d.x();
        p3.z() = start_p.z() + lambda*d.z();

        lambda = (p4.x() - start_p.x())/d.x();
        p4.z() = start_p.z() + lambda*d.z();

    }
    else if(std::abs(d.y())>0){
        double lambda = (p3.y() - start_p.y())/d.y();
        p3.z() = start_p.z() + lambda*d.z();

        lambda = (p4.y() - start_p.y())/d.y();
        p4.z() = start_p.z() + lambda*d.z();

    }
    else{
        return false;
    }


    // find closer point
    Eigen::Vector3d closest_intersection;
    if((p3 - start_p).norm() < (p4 - start_p).norm())
        closest_intersection  = p3;
    else
        closest_intersection = p4;

    if(closest_intersection.z() < center_.z() &&
            closest_intersection.z() > (center_.z()-height_)){
        intersection_point = closest_intersection;
        Eigen::Vector3d v1 = intersection_point-start_p;
        Eigen::Vector3d v2 = end_p-start_p;
        double dot_p = v2.dot(v1);
        double line_length = v2.norm();
        if((dot_p > 0) && (dot_p <= (line_length*line_length)))
            return true;
    }

    return false;
}

void ca_ri::Polar_2D_Representation::SetParams(double angle_resolution, double max_radius, double arc_size, double arc_origin, Eigen::Vector3d &center,
             double height, std::string frame){
    frame_ = frame;
    cells_.clear();
    angle_resolution_ = angle_resolution;
    max_radius_ = max_radius;
    arc_size_ = arc_size;
    center_ = center;
    arc_origin_ = arc_origin;
    height_ = height;
    size_t num_cells = std::ceil(arc_size_/angle_resolution_);
    for(size_t i=0; i<num_cells; i++){
        cells_.push_back(0.0);
    }
}

void ca_ri::Polar_2D_Representation::SetParams(double angle_resolution, double max_radius, double arc_size,Eigen::Vector3d &center){
  cells_.clear();
  angle_resolution_ = angle_resolution;
  max_radius_ = max_radius;
  arc_size_ = arc_size;
  center_ = center;
  arc_origin_ = 0;
  size_t num_cells = std::ceil(arc_size_/angle_resolution_);

  for(size_t i=0; i<num_cells; i++){
      cells_.push_back(0.0);
  }
}

bool ca_ri::Polar_2D_Representation::GetIDs(const Eigen::Vector3d  &query, size_t &id){
    Eigen::Vector3d diff_v = query-center_; diff_v.z() = 0;
    //ROS_ERROR_STREAM("Center::"<<center_.x()<<"::"<<center_.y()<<"::"<<center_.z());
    //ROS_ERROR_STREAM("Query::"<<query.x()<<"::"<<query.y()<<"::"<<query.z());
    double d = diff_v.norm();
    //ROS_INFO_STREAM("d::"<<d);
    if(d > max_radius_)
      return false;

    double angle = Pos2Angle(query);
    //ROS_INFO_STREAM("Angle::"<<angle);
    if((angle < arc_origin_)
            || angle > (arc_size_ + arc_origin_))
        return false;

    id = Angle2CellID(angle);
    //ROS_INFO_STREAM("ID::"<<id);
    return true;
}

bool ca_ri::Polar_2D_Representation::GetValue(const Eigen::Vector3d  &query, double &value){
    double d = center_.norm();
    if(d > max_radius_)
      return false;

    double angle = Pos2Angle(query);
    if(angle < arc_origin_ || angle > (arc_origin_+arc_size_))
        return false;

    size_t id = Angle2CellID(angle);
    value = cells_[id];
    return true;
}

std::vector<std::pair<double, bool> > ca_ri::Polar_2D_Representation::GetValue(const std::vector<Eigen::Vector3d > &query){
//      std::pair<double, bool> p(0,true);
//      std::vector<std::pair<double, bool> > vector_p;
//      for(auto i=0; i< query.size(); i++){
//        vector_p.push_back(p);
//      }
//      return vector_p;
}
