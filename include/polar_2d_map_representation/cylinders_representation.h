/** * @author: AirLab / Field Robotics Center
 *
 * @attention Copyright (C) 2016
 * @attention Carnegie Mellon University
 * @attention All rights reserved
 *
 * @attention LIMITED RIGHTS:
 * @attention The US Government is granted Limited Rights to this Data.
 *            Use, duplication, or disclosure is subject to the
 *            restrictions as stated in Agreement AFS12-1642.
 */
/* Copyright 2017 Sankalp Arora
 * polar_2d_representation.h
 *
 *  Created on: Sep 3, 2017
 *      Author: Sankalp Arora
 */

#ifndef REPRESENTATION_INTERFACE_INCLUDE_CYLINDERS_REPRESENTATION_H
#define REPRESENTATION_INTERFACE_INCLUDE_POLAR_2D_REPRESENTATION_H

#include <representation_interface/representation_interface.h>
#include "visualization_msgs/Marker.h"
#include "ros/ros.h"
namespace ca {
namespace representation_interface{

/**
 * \brief This is an interface class that captures how modules
 * generally interact with the a world representation. The idea
 * is that all the future interactions with a world representation
 * should happen through this basic interface, hence allowing for quick
 * swapping of world representations.
 * For example, we can use this class as an interface for cmu occupancy grid,
 * if one wants to switch to using nea grid, we wrap it in this interface and do not change
 * any of code that uses this interface.
 */
class Polar_2D_Representation: public RepresentationInterface<double,3>{
// Assuming an NED frame
    double angle_resolution_;
    double max_radius_;
    double arc_size_;
    double arc_center_;
    double height_;
    std::string frame_;
    std::vector<double> cells_;
    Eigen::Vector3d center_;

    size_t Angle2CellID(double angle){
        int index = std::ceil((angle - arc_center_)/angle_resolution_);
        if(index < 0){
           //ROS_ERROR_STREAM("Illegal Query in Polar_2D_Representation, with center at::"<<center_);
           index = 0;
        }
        return (size_t)index;
    }

    double CellID2Angle(size_t id){
        //ROS_ERROR_STREAM("ID::"<<id<<" Res::"<<angle_resolution_<<" Center::"<<arc_center_<<" Size::"<<arc_size_);
        double angle = id*angle_resolution_ + arc_center_ + 0.5*angle_resolution_;
        return angle;
    }

    double Pos2Angle(const Eigen::Vector3d &v){
        Eigen::Vector3d angle_vec = v - center_;
        double angle = std::atan2(angle_vec.y(),angle_vec.x());
        return angle;
    }

    std::string frame;
 public:


  Polar_2D_Representation(){
  }

  bool SetID(size_t id, double value){
    if(id>= cells_.size())
        return false;
    cells_[id] = value;
    return true;
  }

  bool IsInCylinder(Eigen::Vector3d pos);

  bool FindIntersection(Eigen::Vector3d start_p, Eigen::Vector3d end_p, double radius, Eigen::Vector3d &intersection_point);

  void SetParams(double angle_resolution, double max_radius, double arc_size, double arc_center, Eigen::Vector3d &center, double height,
                 std::string frame);
  void SetParams(double angle_resolution, double max_radius, double arc_size,Eigen::Vector3d &center);

  ~Polar_2D_Representation(){cells_.clear();}

    bool GetIDs(const Eigen::Vector3d  &query, size_t &id);
  /**
   * \brief initializes the interface class returns false if can't initialize
   * @param
   * @return true if initialization was successful, false otherwise
   */
  virtual bool Initialize(){return true;}

  /** \brief Get value at query location.
   *
   * @param query Eigen column vector representing a general query
   * @return value at location
   */
  virtual bool GetValue(const Eigen::Vector3d  &query, double &value);
  /** \brief Get value at query location.
   *
   * @param query Eigen column vector representing a general query
   * @return value at location
   */
  virtual bool GetValue(const Eigen::Vector3f  &query, double &value){ value = 0; return true;}
  /** \brief Get value at query index.
   *
   * @param query Eigen column vector representing a general query
   * @return value at location
   */
  virtual bool GetValue(const Eigen::Vector3i  &query, double &value){ value = 0; return true;}
  /** \brief Get value at query locations.
   *
   * @param query a vector of Eigen columns vector representing a general query index.
   * @return a vector of values at query locations.
   */
  virtual std::vector<std::pair<double, bool> > GetValue(const std::vector<Eigen::Vector3d > &query);
    /** \brief Get value at query indices.
   *
   * @param query a vector of Eigen columns vector representing general query indices.
   * @return a vector of values at query locations.
   */
  virtual std::vector<std::pair<double, bool> > GetValue(const std::vector<Eigen::Vector3i > &query){ }
  /**
   * \brief Is the query location valid.
   * @param query
   * @return boolean whether the query is valid or not
   */
  virtual bool IsValid(const Eigen::Vector3d  &query){return true; }
  /**
    * \brief Are the query indices valid.
    * @param query
    * @return boolean whether the query is valid or not
    */
  virtual bool IsValid(const Eigen::Vector3i  &query){ return true;}
  /**
   * \brief Returns the minimum valid query that can be made to the representation
   * @param
   * @return minimum valid query location.
   */
  virtual Eigen::Vector3d GetMinQuery(){ return Eigen::Vector3d(-0.5*arc_size_,0,0);}
  /**
   * \brief Returns the maximum valid query
   * @param
   * @return maximum valid query location.
   */
  virtual Eigen::Vector3d GetMaxQuery(){ return Eigen::Vector3d(0.5*arc_size_,0,0);}
  /**
   * \brief Returns the resolution at which the grid locally operates at that location
   * @param query
   * @return resolution
   */
  virtual Eigen::Vector3d GetResolution(const Eigen::Vector3d &query){ return Eigen::Vector3d(angle_resolution_,0,0);}
  /**
   * \brief Returns the resolution at which the grid locally operates at that index
   * @param query
   * @return resolution
   */
  virtual Eigen::Vector3d GetResolution(const Eigen::Vector3i &query){return Eigen::Vector3d(0,0,0);}
  /**
   * \brief returns the frame in which the representation exists,
   * @param
   * @return the frames in which the representation exists
   */
  virtual std::string get_frame(){return frame;}

  visualization_msgs::Marker GetCylinder(int id);
  visualization_msgs::Marker GetSectors(int id);
  void set_frame(std::string temp_frame){frame = temp_frame;}
};

}  // namepsace ca
}  // namespace representation_interface


#endif
